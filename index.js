//const batlib = require('./lib/bat');
const dicelib = require('./lib/dice');
const timelib = require('./lib/time')

module.exports = {
    time: timelib,
    dice: dicelib
};