const Moment = require('moment');
const debug = require('debug')('fenbatlib:time');

let now = () => {
    return new Moment()
};
let format = (moment) => {
    let output = String(((parseInt(moment.hour()) + (parseInt(moment.minute()) + (parseInt(moment.second()) / 60)) / 60) / 24).toFixed(5));
    if (output == '0') {
        output = "0.00000";
    }
    return moment.format('Y.DDDD.') + output.split('.')[1];
}

module.exports = {
    now: now,
    format: format,
    parse: (input) => {
        return format(new Moment(input));
    },
    version: {
        agent: `Node:${process.env.npm_package_name}:${process.env.npm_package_version} (by /u/${process.env.npm_package_author_name})`,
        revision: now().format(),
        irc: 'VERSION ' + process.env.npm_package_name + '(' + process.env.npm_package_version + ')',
        console: process.env.npm_package_name + '(' + process.env.npm_package_version + ')'
    }
};