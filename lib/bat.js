const Moment = require('moment');

var test = () => {
    var times = 10000000;
    var sides = 4;
    cup = [];
    result = [];
    for(var i = 0; i < sides; i++) {
        result.push(0);
    };
    add(times, sides);
    shake();
    for(var i = 0; i < cup.length; i++) {
        result[cup[i].value()-1]++;
    };
    console.log(result);
    var k = result.reduce(deviations, 0);
    var a = k / result.length
    var diff = result.map( (value) => {
        var diff = value - a;
        return diff * diff;
        
    });
    var avgsqr = average(diff);
    var stdDev = Math.sqrt(avgsqr);
    
    
    console.log(` Stdev: ${stdDev.toFixed(3)}   ${((stdDev/times)*100).toFixed(3)}%`);
    cup = [];
    result = null;
}
var deviations = (total, num) => {
    return total + num;
};
function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

module.exports = {};
