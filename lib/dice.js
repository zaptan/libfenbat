const debug = require('debug')('Dice');
const rutil = require('./randomUtil');

const regDice = /(\d*)d(\d+)/i;
const palDice = /(!p)/i;
const palFlag = /([lm])(\d*)/i;
const regFlag = /([!><kx]+)(\d*)([hl])?/i;
const regInva = /[\s'";:\\\][]/i;

const limits = {
    dice: 100,
    sides: 100,
    repeats: 10
};

let parse = (cmd) => {
    let meta = {
        dice: 1,
        sides: 20,
        sets: 1,
        exploding: {target: 0, limit: 0},
        target: {target: 0, highest: true},
        keep: {limit: 0, highest: true},
    };
    if (cmd) {
        if (regInva.test()) {
            return new Error('Can not parse string');
        }
        let answer = regDice.exec(cmd);
        cmd = cmd.replace(regDice, '');
        meta.dice = parseInt(answer[1]);
        meta.sides = parseInt(answer[2]);
        if (palDice.test(cmd)) {
            meta.exploding.group = true;
            meta.exploding.target = 16;
            meta.exploding.limit = 2;
            while (palFlag.test(cmd)) {
                let flag = palFlag.exec(cmd);
                cmd = cmd.replace(palFlag, '');
                switch (flag[1]) {
                case 'm':
                    if (flag[2]) {
                        meta.exploding.min = parseInt(flag[2]);
                    } else {
                        meta.exploding.min = 10;
                    }
                    break;
                case 'l':
                    if (flag[2]) {
                        meta.exploding.limit = parseInt(flag[2]);
                    } else {
                        meta.exploding.limit = 2;
                    }
                    break;
                default:
                }
            }
        } else {
            while (regFlag.test(cmd)) {
                let flag = regFlag.exec(cmd);
                cmd = cmd.replace(regFlag, '');
                switch (flag[1]) {
                case '!!':
                case '!':
                    if (flag[2]) {
                        meta.exploding.target = parseInt(flag[2]);
                    } else {
                        meta.exploding.target = meta.sides;
                    }
                    if (flag[1].length===1) {
                        meta.exploding.limit = 1;
                    } else {
                        meta.exploding.limit = 2;
                    }
                    break;
                case 'k':
                    if (flag[2]) {
                        meta.keep.limit = parseInt(flag[2]);
                    } else {
                        meta.keep.limit = 1;
                    }
                    if (flag[3]==='l') {
                        meta.keep.highest = false;
                    } else {
                        meta.keep.highest = true;
                    }
                    break;
                case '<':
                    meta.target.highest= false;
                    if (flag[2]) {
                        meta.target.target = parseInt(flag[2]);
                    } else {
                        meta.target.target = 1;
                    }
                    break;
                case '>':
                    meta.target.highest= true;
                    if (flag[2]) {
                        meta.target.target = parseInt(flag[2]);
                    } else {
                        meta.target.target = meta.sides;
                    }
                    break;
                case 'x':
                    if (flag[2]) {
                        meta.sets = parseInt(flag[2]);
                    } else {
                        meta.sets = 1;
                    }
                }
            }
        }
        if (meta.sides>limits.sides) return new Error(`Too many sides. Max is (${limits.sides})`);
        if (meta.dice>limits.dice) return new Error(`Too many dice. Max is (${limits.dice})`);
        if (meta.sets>limits.repeats) return new Error(`Too many sets. Max is (${limits.repeats})`);
        if (meta.exploding.target<Math.floor((meta.sides/2)+1)&&(meta.exploding.limit>0)) return new Error('Exploading values can not be more then half the sides');
    }
    return meta;
};

let roll = (sides, minum) => {
    if (typeof sides !== 'number') {
        return new Error('`sides` must be a number');
    }
    return rutil.randPosInt(sides, minum||1)
};

let getSum = (total, num) => total + parseInt(num);
let flatten = (ary) => {
    var ret = [];
    for(var i = 0; i < ary.length; i++) {
        if(Array.isArray(ary[i])) {
            ret = ret.concat(flatten(ary[i]));
        } else {
            ret.push(ary[i]);
        }
    }
    return ret;
};
let totals = (ary) => {
    var ret = [];
    for(var i = 0; i < ary.length; i++) {
        if(Array.isArray(ary[i])) {
            ret.push(ary[i].reduce(getSum, 0));
        } else {
            ret.push(ary[i]);
        }
    }
    return ret;
};

let execute = (cmd) => {
    let final = {status: 200, meta: parse(cmd), verbose: []};
    if (final.meta instanceof Error) return {message: final.meta.message, status: 400, error: 'Invalid Request'};
    let rolls = [];
    var i = 0;
    if (final.meta.exploding.group) {
        let k = 0;
        do {
            rolls = [];
            for (i = 0; i < final.meta.dice; i++) {
                rolls.push(roll(final.meta.sides));
            }
            k = rolls.reduce(getSum, 0);
        } while (k<final.meta.exploding.min);
        final.verbose.push(`total ${k}`);
        i = 0;
        debug('k:',k, (k >= final.meta.exploding.target), (i<final.meta.exploding.limit));
        while (k >= final.meta.exploding.target && i<final.meta.exploding.limit) {
            final.meta.exploding.target = final.meta.sides;
            k = roll(final.meta.sides);
            rolls.push(k);
            final.verbose.push(`add ${k}`);
            i++;
        }
        final.rolls = rolls;
    } else {
        for (i = 0; i < final.meta.dice; i++) {
            rolls.push(roll(final.meta.sides));
        }
        final.verbose.push(rolls.slice());
        if (final.meta.keep.limit>0) {
            var sorted = rolls.slice();
            if (final.meta.keep.highest) {
                final.verbose.push(`keeping ${final.meta.keep.limit} of the highest`);
                sorted.sort((a, b) => b-a);
            } else {
                final.verbose.push(`keeping ${final.meta.keep.limit} of the lowest`);
                sorted.sort((a, b) => a-b);
            }
            final.rolls = sorted.slice(0,final.meta.keep.limit);
        } else {
            final.rolls = rolls;
        }
        if (final.meta.target.target>0) {
            final.hits = 0;
            for (i = 0; i < final.rolls.length; i++) {
                if (final.meta.target.highest) {
                    if (final.rolls[i]>=final.meta.target.target) {
                        final.verbose.push(`[${final.rolls[i]}] hits`);
                        final.hits++;
                    }
                } else {
                    if (final.rolls[i]<=final.meta.target.target) {
                        final.verbose.push(`[${final.rolls[i]}] hits`);
                        final.hits++;
                    }
                }
            }
        }
        if (final.meta.exploding.limit>0) {
            for (i = 0; i < final.rolls.length; i++) {
                if (final.rolls[i]>(final.meta.exploding.target-1)) {
                    var newroll = [], val = 0;
                    val = final.rolls[i];
                    newroll.push(val);
                    do {
                        val = roll(final.meta.sides);
                        newroll.push(val);
                    } while ((final.meta.exploding.limit>1)&&(val>(final.meta.exploding.target-1)));
                    final.verbose.push(`[${final.rolls[i]}] exploded to: [${newroll}]`);
                    final.rolls[i] = newroll;
                }
            }
            final.totals = totals(final.rolls);
        }
    }
    var tmp = flatten(final.rolls);
    final.total = tmp.reduce(getSum, 0);
    return final;
};

module.exports = {parse: parse, roll: roll, execute: execute};
