/*global describe it*/
/*eslint no-unused-vars: ["warn", { "varsIgnorePattern": "should" }]*/
var should = require('chai').should();
var lib = require('../');

describe('time', () => {
    describe('Date', () => {
        it('be a string', () => {
            lib.time.now().format().should.be.a('string');
        });
    });
    describe('Version', () => {
        it(`${lib.time.version.revision}`, () => {
            lib.time.version.revision.should.be.a('string');
        });
        it(`is: ${lib.time.version.console}`, () => {
            lib.time.version.console.should.be.a('string');
        });
    });

});
