/*global describe it*/
/*eslint no-unused-vars: ["warn", { "varsIgnorePattern": "should" }]*/
var should = require('chai').should();
var lib = require('../');

// *** Roll *** //
var r0 = 20;
var r1 = lib.dice.roll(r0);
describe('Dice', () => {
    describe('roll', function() {
        it('be a number', function() {
            lib.dice.roll(r0).should.be.a('number');
        });
        it(`be within 1 and the number of faces provided (${r1})`, function() {
            r1.should.be.within(1, r0).and.satisfy(Number.isInteger);
        });
    });

    // *** Parse *** //
    var p2 = '10d20!11k3l';
    var p3 = '3d6>4x2';
    describe('parse', function() {
        it(p2 + ' should explode with limit 1', function() {
            lib.dice.parse(p2).should.have.property('exploding').that.has.property('limit')
                .that.equals(1);
        });
        it(p2 + ' should keep 3', function() {
            lib.dice.parse(p2).should.have.property('keep').that.has.property('limit')
                .that.equals(3);
        });
        it(p2 + ' should keep the lowest', function() {
            lib.dice.parse(p2).should.have.property('keep').that.has.property('highest')
                .that.equals(false);
        });
        it(p3 + ' should target 4', function() {
            lib.dice.parse(p3).should.have.property('target').that.has.property('target')
                .that.equals(4);
        });
        it(p3 + ' should target higher values', function() {
            lib.dice.parse(p3).should.have.property('target').that.has.property('highest')
                .that.equals(true);
        });
        it(p3 + ' should run 2 times', function() {
            lib.dice.parse(p3).should.have.property('sets').that.equals(2);
        });
        it('if limits are exceeded we should get an error', function() {
            lib.dice.parse('102d102').should.be.instanceof(Error);
        });
        it('stop forever looping exploading dice', function() {
            lib.dice.parse('3d6!!3').should.be.instanceof(Error);
        });
    });

    // *** Execute *** //
    describe('execute', function() {
        it('should return an object', function() {
            lib.dice.execute('1d1').should.be.a('object').that.has.property('total').that.equals(1);
        });
        it('should have verbose lines', function() {
            lib.dice.execute('2d20k1l').should.have.property('verbose').that.is.an('array');
        });
        it('should limit to keep setting', function() {
            lib.dice.execute('10d10k3!!6').should.have.property('totals').that.is.an('array').with.lengthOf(3);
        });
        var e1 = lib.dice.execute('101d10k3!!6');
        it('Pass the Error from parse to execute and return it JSON ready', function() {
            e1.should.be.a('object').with.property('status').that.equals(400);
        });
    });
});
