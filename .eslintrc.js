module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
      "sourceType": "module"
    },
    "rules": {
      "no-unused-vars": ["warn"],
      "no-console": "off",
      "block-spacing": ["error", "never"],
      "indent": ["error", 4],
      "linebreak-style": ["error","unix"],
      "quotes": ["error","single"],
      "semi": ["error","always"],
      "no-mixed-spaces-and-tabs": "error",
      "brace-style": ["error", "1tbs"],
      "no-useless-escape": ["warn"]
    }
}
